const express = require('express');
const bodyParser = require('body-parser');
const router = require('./src/router/userRouter');
const dbConnection = require('./src/config/configDatabase');

const app = express();
app.use(bodyParser.json());

router(app);

// Iniciar el servidor
const PORT = 3000;
app.listen(PORT, async () => {
  await dbConnection();
  console.log(`Servidor iniciado en el puerto ${PORT}`);
});