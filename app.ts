// const express = require('express');
// const mongoose = require('mongoose');
// const bodyParser = require('body-parser');

// const app = express();
// app.use(bodyParser.json());

// // Conectar a la base de datos MongoDB
// async function connect() {
//   mongoose.connect('mongodb://127.0.0.1:27017/mydatabase', {
//   useNewUrlParser: true, // analizador de url en mongoDB
//   useUnifiedTopology: true
// });
// console.log('Conectado a la base');
// }


// // Definir el esquema y el modelo de Mongoose
// const userSchema = new mongoose.Schema({
//   name: String,
//   age: Number
// });
// const User = mongoose.model('users', userSchema);

// // Rutas del CRUD

// // Obtener todos los usuarios
// app.get('/users', async (req: any, res: any) => {
//   try {
//     const users = await User.find();
//     res.json(users);
//   } catch (error: any) {
//     res.status(500).json({ error: error.message });
//   }
// });

// // Obtener un usuario por su ID
// app.get('/users/:id', async (req: any, res: any) => {
//   try {
//     const user = await User.findById(req.params.id);
//     if (!user) {
//       return res.status(404).json({ message: 'Usuario no encontrado' });
//     }
//     res.json(user);
//   } catch (error: any) {
//     res.status(500).json({ error: error.message });
//   }
// });

// // Crear un nuevo usuario
// app.post('/users', async (req: any, res: any) => {
//   try {
//     const user = new User(req.body);
//     await user.save();
//     res.status(201).json(user);
//   } catch (error: any) {
//     res.status(500).json({ error: error.message });
//   }
// });

// // Actualizar un usuario por su ID
// app.put('/users/:id', async (req: any, res: any) => {
//   try {
//     const user = await User.findByIdAndUpdate(req.params.id, req.body, {
//       new: true
//     });
//     if (!user) {
//       return res.status(404).json({ message: 'Usuario no encontrado' });
//     }
//     res.json(user);
//   } catch (error: any) {
//     res.status(500).json({ error: error.message });
//   }
// });

// // Eliminar un usuario por su ID
// app.delete('/users/:id', async (req: any, res: any) => {
//   try {
//     const user = await User.findByIdAndDelete(req.params.id);
//     if (!user) {
//       return res.status(404).json({ message: 'Usuario no encontrado' });
//     }
//     res.json({ message: 'Usuario eliminado correctamente' });
//   } catch (error: any) {
//     res.status(500).json({ error: error.message });
//   }
// });

// // Iniciar el servidor
// const PORT = 3000;
// app.listen(PORT, async () => {
//   await connect();
//   console.log(`Servidor iniciado en el puerto ${PORT}`);
// });
