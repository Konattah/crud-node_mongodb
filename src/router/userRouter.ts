const { Router } = require('express');
const userController = require('../controller/user-controller');

function userRoutes(app: any){
    const router = Router();
    
    router.get('/users', userController.getAllUsersController);
    router.get('/users/:id', userController.userByIdController);
    router.post('/users', userController.newUserController);
    router.put('/users/:id', userController.updateUserByIdController);
    router.delete('/users/:id', userController.deleteUserByIdController);

    app.use(router);
}

module.exports = userRoutes;