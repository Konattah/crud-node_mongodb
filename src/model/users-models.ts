const { Schema, model } = require('mongoose');

// Definir el esquema y el modelo de Mongoose

const userSchema = Schema({
    name: String,
    age: Number
});

module.exports = model('Usuario', userSchema);