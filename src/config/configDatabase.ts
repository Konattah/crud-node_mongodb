const mongoose = require('mongoose');

// Conectarse a la base de datos mongoDB con mongoose
async function conexionBase(){
    mongoose.connect('mongodb://127.0.0.1:27017/mydatabase', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  console.log('Conectado a la base');
}

module.exports = conexionBase;