const userModel = require('../model/users-models');

// Obtener todos los usuarios
async function getAllUsers() {
    try {
        const users = await userModel.find();
        return users;
      } catch (error: any) {
        throw new Error('Error al obtener todos los usuarios de la base de datos.');
      }
}

// Obtener un usuario por su ID
async function userById(id: any) {
    try{
        const user = await userModel.findById(id);
        return user;
    } catch(error){
        throw new Error('Error al obtener al usuario por su ID.');
    }
}

// Crear un nuevo usuario
async function newUser(data: any) {
    try {
        const user = new userModel(data);
        const userSaved = await user.save();
        return userSaved;
      } catch (error: any) {
        throw new Error('Error al crear a un ususario.');
      }
}
  
// Actualizar un usuario por su ID
async function updateUserById(id: any, data: any) {
    try {
        const userUpdated = await userModel.findByIdAndUpdate(id, data, { new: true });
        return userUpdated;
    } catch (error: any) {
        throw new Error('Error al actualizar a un ususario.');
    }
}
  
// Eliminar un usuario por su ID
async function deleteUserById(id: any) {
    try {
        await userModel.findByIdAndDelete(id);
    } catch (error: any) {
        throw new Error('Error al eliminar a un ususario.');
    }
}

module.exports = {
    getAllUsers,
    userById,
    newUser,
    updateUserById,
    deleteUserById
}